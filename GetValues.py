#!/usr/bin/env python

# example base.py

import pygtk
pygtk.require('2.0')
import gtk

class GetValues(object):

    def string_callback(self, widget, data):
        print('value is %s ' % widget.get_text())

    def number_callback(self, widget, data):
        try:
            out = float(widget.get_text())
            print('value is {0}'.format(out))
        except ValueError:
            print('not a number')

    def multi_callback(self, widget, number):
        out = widget.get_text().split()
        if len(out) > number:
            print('too many values')
        for n in range(0,number):
            try:
                print('value is {0}'.format(float(out[n])))
            except ValueError:
                print('please enter a number')

    def delete_event(self,widget, event, data=None):
        print('Delete Event occured')
        gtk.main_quit()
        return False

    def destroy(self,widget, data=None):
        gtk.main_quit()

    def entry_toggle_visibility(self, checkbutton, entry):
        entry.set_editable(checkbutton.get_active())

    def input_field(self, label, set_text, xalign, yalign, call, number = None):
        # Make label and entry
        box = gtk.HBox(False,0)
        label = gtk.Label(label)
        label.set_justify(gtk.JUSTIFY_LEFT)
        label.set_line_wrap(True)
        label.set_alignment(0,0)
        label.show()

        entry = gtk.Entry()
        entry.set_text(set_text)
        entry.set_max_length(50)
        entry.connect("activate", call, number)
        entry.show()

        box.pack_start(label)
        box.pack_start(entry)
        box.set_homogeneous(True)
        box.show()
        return box

    def __init__(self):
        # create a window
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.set_size_request(800,800)
        window.set_title("NineSim")
        window.connect('delete_event',self.delete_event)        
        window.set_border_width(20)

        #add a table to the window.
        table = gtk.Table(3,6,False)
        window.add(table)
        #make a notebook and attach it to the table
        notebook = gtk.Notebook()
        notebook.set_tab_pos(gtk.POS_TOP)
        table.attach(notebook,0,6,0,1)
        notebook.show() 
        #add page numbers to tab
        for i in range 5:
            frame_num = "frame # {}".format(i+1)
            page_num = "Page # {}".format(i+1)
            frame = gtk.Frame(frame_num)
            frame.show()
            label = gtk.Label(frame_num)
        #labels and fields for vbox 
        vbox = gtk.VBox(True,0)
        vbox.set_homogeneous(True)
        vbox.show()
        
        val1_field = self.input_field('Description',"",0,0,self.string_callback)
        val2_field = self.input_field('Interact Type',"",0,0,self.string_callback)
        val3_field = self.input_field('Loc. (X and Y to GNDA GPS)','# #',0,0,self.multi_callback,2)
        val4_field = self.input_field('velocity','#',0,0,self.number_callback)
        val5_field = self.input_field('Result Dist Id','#',0,0,self.number_callback)
        val6_field = self.input_field('Params Id','#',0,0,self.number_callback)
        val7_field = self.input_field('N.E. Id','# # #',0,0,self.multi_callback,3)
        vbox.pack_start(val1_field)
        vbox.pack_start(val2_field)
        vbox.pack_start(val3_field)
        vbox.pack_start(val4_field)
        vbox.pack_start(val5_field)
        vbox.pack_start(val6_field)
        vbox.pack_start(val7_field)
        
        box1 = gtk.HBox(False,0)
        button1 = gtk.Button("Button 1")
        box1.pack_start(button1, True,True, 40)
        button1.connect("clicked",self.string_callback,"button 1")
        button1.show()

        button2 = gtk.Button("button2")
        box1.pack_start(button2, True,True,40)
        button2.connect('clicked', self.string_callback, "button 2")
        #self.window.add(self.button)
        button2.show()
        # make a tab
        #box2 = gtk.HBox(False,0)
        #box2.pack_start(,True,True,40)
        #pack box 1 in vbox
        vbox.pack_start(box1,True,True,0)
        box1.show()
    
        window.add(vbox)
        window.show()
    
        

    def main(self):
        gtk.main()

print( __name__)
if __name__ == "__main__":
    getvalues = GetValues()
    getvalues.main()

