#!/usr/bin/env python

# example base.py

import pygtk
pygtk.require('2.0')
import gtk

class GetValues(object):

    def string_callback(self, widget, data):
        print('value is %s ' % widget.get_text())

    def number_callback(self, widget, data):
        try:
            out = float(widget.get_text())
            print('value is {0}'.format(out))
        except ValueError:
            print('not a number')

    def multi_callback(self, widget, number):
        out = widget.get_text().split()
        if len(out) > number:
            print('too many values')
        for n in range(0,number):
            try:
                print('value is {0}'.format(float(out[n])))
            except ValueError:
                print('please enter a number')

    def delete_event(self,widget, event, data=None):
        print('Delete Event occured')
        gtk.main_quit()
        return False

    def destroy(self,widget, data=None):
        gtk.main_quit()

    def entry_toggle_visibility(self, checkbutton, entry):



        entry.set_editable(checkbutton.get_active())

    def input_field(self, label, set_text, xalign, yalign, call, number = None):
        # Make label and entry
        box = gtk.HBox(False,0)
        label = gtk.Label(label)
        label.set_justify(gtk.JUSTIFY_LEFT)
        label.set_line_wrap(True)
        label.set_alignment(0,0)
        label.show()

        entry = gtk.Entry()
        entry.set_text(set_text)
        entry.set_max_length(50)
        entry.connect("activate", call, number)
        entry.show()

        box.pack_start(label)
        box.pack_start(entry)
        box.set_homogeneous(True)
        box.show()
        return box

    def pagefive(self):
        vbox = gtk.VBox(True,0)
        vbox.set_homogeneous(True)
        vbox.show()
        val1_field = self.input_field("stuff","",0,0,self.string_callback)
        val3_field = self.input_field('Loc. (X and Y to GNDA GPS)','# #',0,0,self.multi_callback,2)
        val4_field = self.input_field('velocity','#',0,0,self.number_callback)
        val5_field = self.input_field('Result Dist Id','#',0,0,self.number_callback)
        val6_field = self.input_field('Params Id','#',0,0,self.number_callback)
        val2_field = self.input_field('Interact Type',"",0,0,self.string_callback)
        val7_field = self.input_field('N.E. Id','# # #',0,0,self.multi_callback,3)
        vbox.pack_start(val1_field)
        vbox.pack_start(val2_field)
        vbox.pack_start(val3_field)
        vbox.pack_start(val4_field)
        vbox.pack_start(val5_field)
        vbox.pack_start(val6_field)
        vbox.pack_start(val7_field)
        return vbox

    def __init__(self):
        # create a window
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.set_size_request(800,800)
        window.set_title("NineSim")
        window.connect('delete_event',self.delete_event)        
        window.set_border_width(20)

        #add a table to the window.
        table = gtk.Table(4,9,False)
        window.add(table)
        #make a notebook and attach it to the table
        notebook = gtk.Notebook()
        notebook.set_tab_pos(gtk.POS_TOP)
        table.attach(notebook,0,7,0,3)
        notebook.show() 
        tab_name = [ "Scenario","Distributions","Conditions", "Detector", "Interactions","Output"]
        lanes = ["Lane 1","Lane 2","Lane 3","Lane 4","Lane 5","Lane 6","Lane 7","Lane 8"]
        #add page numbers to tab
        for x, name in enumerate(tab_name):
            
            frame_num = x
            page_name = name
            frame = gtk.Frame(frame_num)
            frame.show()
            label = gtk.Label(frame_num)
            if x == 4:
                vbox = self.pagefive()
                notebook.append_page(vbox,label)             
            
            else:
                frame.set_shadow_type(gtk.SHADOW_OUT)
                frame.add(label)
                label.show()
                label = gtk.Label(page_name)
                notebook.append_page(frame,label)             

       # # adds a button to page
       # button1 = gtk.Button("Button 1")
       # button1.connect("clicked",self.string_callback,"button 1")
       # label = gtk.Label("button1")
       # button1.show()
       # notebook.insert_page(button1,None,2)
        #stating page
        notebook.set_current_page(0)
        #make some bottons
        buttont2 = gtk.Button("buttont2")
        buttont2.connect("clicked", self.string_callback, "button 2")
        table.attach(buttont2,0,1,0,1)
        #self.window.add(self.button)
        buttont2.show()
        table.show()


        #box1.pack_start(button2, True,True,40)
        #labels and fields for vbox 
#        
        #box1.pack_start(button1, True,True, 40)
#        box1 = gtk.HBox(False,0)
#
#        # make a tab
#        #box2 = gtk.HBox(False,0)
#        #box2.pack_start(,True,True,40)
#        #pack box 1 in vbox
#        vbox.pack_start(box1,True,True,0)
#        box1.show()
#    
#        window.add(vbox)
        window.show()
    
        

    def main(self):
        gtk.main()

print( __name__)
if __name__ == "__main__":
    getvalues = GetValues()
    getvalues.main()

